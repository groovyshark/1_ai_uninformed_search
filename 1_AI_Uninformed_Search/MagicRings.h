//
// Created by Vyacheslav on 21.02.2016.
//

#ifndef MAGICRINGS_H
#define MAGICRINGS_H

#include <iostream>
#include <vector>
#include <list>
#include <stack>
#include <algorithm>
#include <fstream>
#include <sstream>

typedef std::vector <char> Rings;

const int countBalls = 9;
const int countRings = 2;

class MagicRings {
public:
	MagicRings();
	MagicRings(Rings startState);
	MagicRings(std::string);

	void LeftClockwiseTurn();
	void RightClockwiseTurn();
	void LeftCounterClockwiseTurn();
	void RightCounterClockwiseTurn();

	std::list <MagicRings> GetCurrentStates();
	//std::list <MagicRings> GetCurrentStates();

	void DisplayState();
	std::string ToString();

	void ReadFromFile(std::string);
	void WriteToFile(std::string);

	inline int Size()
	{
		return rings.size();
	}

	char at(int i)
	{
		return rings[i];
	}

	inline friend bool operator==(const MagicRings& left, const MagicRings& right)
	{
		return left.rings == right.rings;
	}

private:
	Rings rings;
};


#endif //MAGICRINGS_H
