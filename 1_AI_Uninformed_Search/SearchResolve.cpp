#include "SearchResolve.h"

TreeNode* SearchResolve::DFS()
{
	TreeNode* root = new TreeNode(start, nullptr);
	std::deque <TreeNode*> openNodes;
	std::list <TreeNode*> closeNodes;

	openNodes.push_back(root);

	long count = 0;
	while (!openNodes.empty())
	{
		++count;
		TreeNode* current = openNodes.front();
		openNodes.pop_front();

		if (current->GetState() == result)
		{
			std::cout << "Variants enumerated: " << count - 1 << std::endl;
			return current;
		}

		closeNodes.push_back(current);
		
		std::list <MagicRings> turns = current->GetState().GetCurrentStates();
		
		for (auto item : turns)
		{
			TreeNode* child = new TreeNode(item, current);
			if (std::find(openNodes.begin(), openNodes.end(), child) == openNodes.end() &&
				std::find(closeNodes.begin(), closeNodes.end(), child) == closeNodes.end())
				openNodes.push_back(child);
		}
		
	}
	return nullptr;
}

TreeNode* SearchResolve::HeuristicSearch()
{
	TreeNode* root = new TreeNode(start, nullptr);
	root->Cost(CalculateHeuristic(root->GetState()));

	std::deque <TreeNode*> openNodes;
	std::list <TreeNode*> closeNodes;

	openNodes.push_back(root);

	long count = 0;
	while (!openNodes.empty())
	{
		++count;
		TreeNode* current = openNodes.front();
		openNodes.pop_front();

		if (current->GetState() == result)
		{
			std::cout << "Variants enumerated: " << count - 1 << std::endl;
			return current;
		}

		closeNodes.push_back(current);

		std::list <MagicRings> turns = current->GetState().GetCurrentStates();
		for (auto state : turns)
		{
			TreeNode* child = new TreeNode(state, current);
			child->Cost(CalculateHeuristic(child->GetState()));

			if (std::find(openNodes.begin(), openNodes.end(), child) == openNodes.end() &&
				std::find(closeNodes.begin(), closeNodes.end(), child) == closeNodes.end())
			{
				if (openNodes.size() >= memoryLimit)
					LimitMemory(openNodes);

				openNodes.push_back(child);
			}

			else if (std::find(openNodes.begin(), openNodes.end(), child) != openNodes.end())
			{
				for (auto node : openNodes)
				{
					if (node == child && node->Cost() < child->Cost())
					{
						child->Cost(node->Cost());
						break;
					}
				}
			}
			else if (std::find(closeNodes.begin(), closeNodes.end(), child) != closeNodes.end())
			{
				for (auto node : closeNodes)
				{
					if (node == child && node->Cost() < child->Cost())
					{
						child->Cost(node->Cost());
						closeNodes.remove(node);

						if (openNodes.size() >= memoryLimit)
							LimitMemory(openNodes); 

						openNodes.push_back(child);
					}
				}
			}
		}

		std::sort(openNodes.begin(), openNodes.end(), [](TreeNode* a, TreeNode* b) 
		{
			return a->Cost() < b->Cost();
		});
	}

	return nullptr;
}

int SearchResolve::CalculateHeuristic(MagicRings& const state)
{
	int count = 0, size = result.Size();

	for (int i = 0; i < size; ++i)
		if (state.at(i) != result.at(i))
			++count;

	return count;
}

void SearchResolve::LimitMemory(std::deque <TreeNode*>& src)
{
	std::sort(src.begin(), src.end(), [](TreeNode* a, TreeNode* b)
	{
		return a->Cost() < b->Cost();
	});

	src.pop_back();
	std::cout << "Memory bound!" << std::endl;
}