#ifndef SEARCHPROBLEM_H
#define SEARCHPROBLEM_H

#include "MagicRings.h"
#include "TreeNode.h"
#include <stack>
#include <Windows.h>
#include <stdio.h>
#include <conio.h>
#include <ctime>

class SearchResolve
{
public:
	SearchResolve()
	{
	}

	SearchResolve(MagicRings& start_, MagicRings& _result) :
		start(start_),
		result(_result)
	{
	}
	
	TreeNode* DFS();
	TreeNode* HeuristicSearch();
	
	int CalculateHeuristic(MagicRings& const);

	
private:
	void LimitMemory(std::deque <TreeNode*>&);
	
	MagicRings start;
	MagicRings result;

	const int memoryLimit = 30;
};

#endif //SEARCHPROBLEM_H
