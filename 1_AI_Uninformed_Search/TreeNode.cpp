#include "TreeNode.h"

void TreeNode::PrintWay()
{
	std::stack <TreeNode*> way;
	TreeNode* current = this;

	while (current != nullptr)
	{
		way.push(current);
		current = current->parent;
	}

	auto turnsCount = way.size();
	while (!way.empty())
	{
		way.top()->GetState().DisplayState();
		std::cout << std::endl;
		way.pop();
	}
	std::cout << std::endl << "Turns count: " << turnsCount << std::endl;
}
