#ifndef TREENODE_H
#define TREENODE_H

#include "MagicRings.h"

class TreeNode
{
public:

	TreeNode()
	{
	}

	TreeNode(const MagicRings& _state, TreeNode* _parent) :
		state(_state),
		parent(_parent),
		cost(0)
	{
	}

	MagicRings GetState() 
	{ 
		return state; 
	}

	int Cost() { return cost; }
	void Cost(int _cost) { cost = _cost; }

	void PrintWay();

private:
	MagicRings state;
	TreeNode* parent;

	int cost;
};

#endif

