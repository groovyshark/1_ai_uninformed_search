#include "MagicRings.h"

MagicRings::MagicRings()
{
	rings.resize(countBalls * countRings);

	int i = 0;
	for (; i < 9; ++i)
		rings[i] = 'R';

	for (; i < 18; ++i)
		rings[i] = 'B';

	std::random_shuffle(rings.begin(), rings.end());
}

MagicRings::MagicRings(Rings startState)
{
	rings.resize(startState.size());

	std::copy(startState.begin(), startState.end(), rings.begin());
}

MagicRings::MagicRings(std::string filename)
{
	ReadFromFile(filename);
}

void MagicRings::DisplayState()
{
	std::cout << "   " << rings[0] << rings[1] << "     " << rings[2] << rings[3];
	std::cout << std::endl;

	std::cout << " " << rings[4] << "     " << rings[5] << "     " << rings[6];
	std::cout << std::endl;

	std::cout << rings[7] << "     " << rings[8] << " " << rings[9] << "     " << rings[10];
	std::cout << std::endl;

	std::cout << " " << rings[11] << "     " << rings[12] << "     " << rings[13];
	std::cout << std::endl;

	std::cout << "    " << rings[14] << rings[15]<< "    " << rings[16] << rings[17];
	std::cout << std::endl;
}

void MagicRings::LeftClockwiseTurn()
{
	Rings tempRings(rings);

	rings[11] = tempRings[14];
	rings[7] = tempRings[11];
	rings[4] = tempRings[7];
	rings[0] = tempRings[4];
	rings[1] = tempRings[0];
	rings[5] = tempRings[1];
	rings[9] = tempRings[5];
	rings[12] = tempRings[9];
	rings[15] = tempRings[12];
	rings[14] = tempRings[15];

}

void MagicRings::LeftCounterClockwiseTurn()
{
	Rings tempRings(rings);

	rings[14] = tempRings[11];
	rings[11] = tempRings[7];
	rings[7] = tempRings[4];
	rings[4] = tempRings[0];
	rings[0] = tempRings[1];
	rings[1] = tempRings[5];
	rings[5] = tempRings[9];
	rings[9] = tempRings[12];
	rings[12] = tempRings[15];
	rings[15] = tempRings[14];
}

void MagicRings::RightClockwiseTurn()
{
	Rings tempRings(rings);

	rings[12] = tempRings[16];
	rings[8] = tempRings[12];
	rings[5] = tempRings[8];
	rings[2] = tempRings[5];
	rings[3] = tempRings[2];
	rings[6] = tempRings[3];
	rings[10] = tempRings[6];
	rings[13] = tempRings[10];
	rings[17] = tempRings[13];
	rings[16] = tempRings[17];
}

void MagicRings::RightCounterClockwiseTurn()
{
	Rings tempRings(rings);

	rings[16] = tempRings[12];
	rings[12] = tempRings[8];
	rings[8] = tempRings[5];
	rings[5] = tempRings[2];
	rings[2] = tempRings[3];
	rings[3] = tempRings[6];
	rings[6] = tempRings[10];
	rings[10] = tempRings[13];
	rings[13] = tempRings[17];
	rings[17] = tempRings[16];
}

std::list <MagicRings> MagicRings::GetCurrentStates()
{
	std::list <MagicRings> turns;

	MagicRings state(this->rings);

	state.RightCounterClockwiseTurn();
	turns.push_back(state);
	state.RightClockwiseTurn();

	state.LeftClockwiseTurn();
	turns.push_back(state);
	state.LeftCounterClockwiseTurn();

	state.RightClockwiseTurn();
	turns.push_back(state);
	state.RightCounterClockwiseTurn();

	state.LeftCounterClockwiseTurn();
	turns.push_back(state);
	//state.LeftClockwiseTurn();

	return turns;
}

std::string MagicRings::ToString()
{
	std::stringstream sout;
	for (auto item : rings)
	{
		sout << item << " ";
	}
	return sout.str();
}

void MagicRings::ReadFromFile(std::string filename)
{
	std::ifstream fin(filename);

	rings.clear();
	while (!fin.eof())
	{
		char item;
		fin >> item;
		rings.push_back(item);
	}
	rings.pop_back();
	fin.close();
}

void MagicRings::WriteToFile(std::string filename)
{
	std::ofstream fout(filename);

	fout << ToString();

	fout.close();
}