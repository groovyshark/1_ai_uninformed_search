#include <iostream>
#include "SearchResolve.h"

int main()
{

	Rings result { 'R', 'R', 'B', 'B', 'R', 'B', 'B', 'R', 'B',
		'R', 'B', 'R', 'R', 'B', 'R', 'R', 'B', 'B' };

	MagicRings start("source.txt");
	//start.WriteToFile("source.txt");
	MagicRings resultRings(result);

	SearchResolve search(start, resultRings);

	std::cout << "Initial state:" << std::endl;
	start.DisplayState();

	std::cout << std::endl << "Searching..." << std::endl;
	auto startTime = clock();
	auto resultNode = search.DFS();
	auto endTime = clock();

	if (resultNode != nullptr)
	{
		std::cout << "Solution found\n" << std::endl;
		resultNode->PrintWay();
	}
	else
		std::cout << "Solution not found" << std::endl;

	std::cout << "Elapsed time: " <<(endTime - startTime ) / 1000.0<< " sec." << std::endl;

	system("pause");
	return 0;
}